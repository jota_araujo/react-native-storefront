# Description

A mobile storefront featuring list animations and shared elements between screens.

### Technologies used in this project:

- [React Native Reanimated 2.3](https://docs.swmansion.com/react-native-reanimated/)
- [React Native Gesture Handler](https://docs.swmansion.com/react-native-gesture-handler/)
- [Expo SDK 44](https://expo.dev/)

![screencapture](https://bitbucket.org/jota_araujo/react-native-storefront/raw/f1371f14e1bb983354f5ba7f9effb0871407b220/storefront_capture.gif)
