import { useAnimatedStyle, useSharedValue } from "react-native-reanimated";

type ProgressInitialValue = 0 | 1;

export const useAnimatedProgress = (
  propName: String,
  initialValue: ProgressInitialValue = 0
) => {
  const progress = useSharedValue<number>(initialValue);

  const style = useAnimatedStyle(() => {
    return {
      [`${propName}`]: progress.value,
    };
  });
  return { progress, style };
};

export const useAnimatedTransform = (
  propName: String,
  initialValue: number
) => {
  const animation = useSharedValue<number>(initialValue);

  const style = useAnimatedStyle(() => {
    return {
      transform: [
        {
          [`${propName}`]: animation.value,
        },
      ],
    } as any;
  });

  return { animation, style };
};
