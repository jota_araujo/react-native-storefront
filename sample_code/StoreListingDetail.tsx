import { useNavigation, useRoute } from "@react-navigation/native";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Pressable,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { StoreListingItem } from "./mocks";
import Box from "./Box";
import { SharedElement } from "react-navigation-shared-element";
import { StoreListingRouteParams } from "./types";
import { theme } from "./constants";
import { useAnimatedProgress } from "./hooks/hooks";
import { useEffect } from "react";
import { Easing, withDelay, withTiming } from "react-native-reanimated";
import { setNavigationBarColor } from "./utils";

const { width: screenW, height: screenH } = Dimensions.get("window");

export default function StoreListingDetail() {
  const { params } = useRoute();
  const { goBack } = useNavigation();
  const { progress, style: animatedOpacity } = useAnimatedProgress("opacity");

  useEffect(() => {
    setNavigationBarColor(theme.colors.darkest, true);
    progress.value = withDelay(
      400,
      withTiming(1, { duration: 400, easing: Easing.out(Easing.linear) })
    );
  }, []);

  const { id, title, image, author, description } = (params as any)
    .item as StoreListingItem;

  return (
    <View style={styles.container}>
      <SharedElement id={id}>
        <Image source={image} resizeMode="cover" style={styles.image} />
      </SharedElement>

      <Box absolute top={48} left={16}>
        <Pressable style={styles.circle} onPress={() => goBack()}>
          <Ionicons name="arrow-back-sharp" size={24} color="white" />
        </Pressable>
      </Box>
      <Box animated style={animatedOpacity} padding={theme.padding.large}>
        <Text style={styles.title}>{`${title}`}</Text>
        <Text style={styles.author}>{`${author}`}</Text>
        <Text style={styles.description}>{`${description}`}</Text>
      </Box>
    </View>
  );
}

type RouteParams = {
  params: StoreListingRouteParams;
};

StoreListingDetail.sharedElements = ({ params }: RouteParams) => {
  const { item } = params;
  return [
    {
      id: item.id,
    },
  ];
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: theme.colors.dark,
  },
  image: {
    width: screenW,
    height: screenH * 0.6,
  },
  title: {
    ...theme.typography.title,
    ...theme.typography.lightText,
  },
  author: {
    ...theme.typography.subTitle,
    color: theme.colors.highlight,
  },
  description: {
    ...theme.typography.body,
    ...theme.typography.lightText,
  },
  circle: {
    width: 54,
    height: 54,
    borderRadius: 27,
    backgroundColor: "#00000040",
    alignItems: "center",
    justifyContent: "center",
  },
});
