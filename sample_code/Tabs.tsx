import React, { memo, useEffect, useState } from "react";
import { StyleSheet, View, Dimensions, Text, Pressable } from "react-native";
import Animated, { useSharedValue, withSpring } from "react-native-reanimated";

import Box from "./Box";
import { theme } from "./constants";
import { useAnimatedTransform } from "./hooks/hooks";

const screenW = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    width: screenW,
  },
  selectionContainer: {
    position: "absolute",
    left: screenW * 0.05,
    width: screenW * 0.3,
    height: 2,
    backgroundColor: theme.colors.highlight,
  },
  tabTitle: {
    fontSize: theme.typography.subTitle.fontSize,
    ...theme.typography.lightText,
    textAlign: "center",
  },
  box: {
    justifyContent: "space-around",
  },
});

interface TabSelectionProps {
  index: number;
}

const TabSelection = memo(({ index }: TabSelectionProps) => {
  const { animation, style: animatedOffset } = useAnimatedTransform(
    "translateX",
    0
  );

  useEffect(() => {
    animation.value = withSpring(screenW * 0.3 * index, {
      overshootClamping: true,
    });
  }, [index]);

  return (
    <Animated.View
      style={[styles.selectionContainer, animatedOffset]}
    ></Animated.View>
  );
});

interface TabTitleProps {
  onPress: () => void;
  text: string;
}

const TabTitle = memo(({ text, onPress }: TabTitleProps) => {
  const progress = useSharedValue(0);
  return (
    <Pressable style={{ width: screenW * 0.3 }} onPress={onPress}>
      <Text style={styles.tabTitle}>{text}</Text>
    </Pressable>
  );
});

interface TabProps {
  onTabChange: (n: number) => void;
}

export default memo(({ onTabChange }: TabProps) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState<number>(0);

  return (
    <View style={[styles.container]}>
      <Box
        hz
        style={{
          justifyContent: "space-around",
          width: screenW * 0.95,
          paddingLeft: screenW * 0.05,
          paddingBottom: theme.padding.medium,
        }}
      >
        <TabTitle
          text="Popular Now"
          onPress={() => {
            setSelectedTabIndex(0);
            onTabChange(0);
          }}
        />
        <TabTitle
          text="New Releases"
          onPress={() => {
            setSelectedTabIndex(1);
            onTabChange(1);
          }}
        />
        <TabTitle
          text="Top Rated"
          onPress={() => {
            setSelectedTabIndex(2);
            onTabChange(2);
          }}
        />
      </Box>
      <Box
        hz
        style={{
          alignItems: "space-between",
          height: theme.padding.extraLarge * 2,
        }}
      >
        <View
          style={{
            //...StyleSheet.absoluteFillObject,
            width: screenW,
            height: theme.padding.extraLarge,
            borderTopLeftRadius: theme.padding.extraLarge,
            borderTopRightRadius: theme.padding.extraLarge,
            backgroundColor: theme.colors.darkest,
          }}
        />
        <TabSelection index={selectedTabIndex} />
      </Box>
    </View>
  );
});
