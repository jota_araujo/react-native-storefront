import * as NavigationBar from "expo-navigation-bar";
import { ColorValue } from "react-native";

export function setNavigationBarColor(color: ColorValue, isDark: boolean) {
  NavigationBar.setBackgroundColorAsync(color);
  NavigationBar.setButtonStyleAsync(isDark ? "light" : "dark");
}
